import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { NewsListComponent } from 'src/app/components/news/news-list/news-list.component';
import { NewsDetailComponent } from 'src/app/components/news/news-detail/news-detail.component';
import { NewsFormComponent } from 'src/app/components/news/news-form/news-form.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/news',
    pathMatch: 'full'
  },
  {
    path: 'news',
    component: NewsListComponent,
  },
  {
    path: 'news/:id/view',
    component: NewsDetailComponent
  },
  {
    path: 'news/add',
    component: NewsFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
