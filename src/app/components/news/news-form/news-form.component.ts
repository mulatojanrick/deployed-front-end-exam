import { Component, OnInit } from '@angular/core';

import { PostService } from 'src/app/services/post.service';
import { Post } from 'src/app/models/post.model';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-news-form',
  templateUrl: './news-form.component.html',
  styleUrls: ['./news-form.component.css']
})
export class NewsFormComponent implements OnInit {

  news = new Post();

  constructor(private postService: PostService,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  /**
   * Clear news re-instantiate new post
   *
   */
  clearNews() {
    this.news = new Post();
  }

  /**
   * Submit the form to
   *
   */
  submitForm() {
    this.postService.addPost(this.news)
    .then(res => {
        this.clearNews();
        this._snackBar.open('Successfully added post', 'Ok', {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right'
        });
      }
    );
  }
}
