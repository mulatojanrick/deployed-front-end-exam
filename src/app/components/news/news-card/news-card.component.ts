import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Post } from 'src/app/models/post.model';
@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.css']
})
export class NewsCardComponent implements OnInit {

  @Input() post: Post;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  /**
   * Redirect to news with id parameter using post input
   *
   */
  goToNewsDetail() {
    this.router.navigate(['/news/', this.post?.id, 'view'])
  }
}
